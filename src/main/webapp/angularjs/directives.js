app
.directive('printCard', function() {
	return {
		restrict: "E",
		scope: {
			'cardId': '=',
			'cardClick': '&',
			'addCardToDeck': '&',
			'count': '=',
			'deckSize': '=',
			'subtractCardFromDeck': '&'
		},
		link: function(scope, element, attrs) {
			
			scope.$watch('deckSize', function() {
				scope.allowAdd = scope.count < 4 && scope.deckSize < 60;
				scope.allowSubtract = scope.count > 0;
			});
			
			scope.selectCard = function() {
				scope.cardClick();
			}
			scope.addCard = function () {
				scope.addCardToDeck();
			}
			scope.subtractCard = function() {
				scope.subtractCardFromDeck();
				console.log("Usuwam: " + scope.allowSubtract);
			}
		},
		templateUrl: "angularjs/print-card.html"
	}
})

.directive('printYourCard',['$rootScope', function($rootScope) {
	return {
		restrict: "E",
		scope: {
			'pokemon': '=',
			'cacheCards': '=',
			'addYourCard': '&',
			'subtractYourCard': '&',
			'deckSize': '='
		},
		link: function(scope, element, attrs) {
			scope.cards = $rootScope.cache.cards;
			
			scope.$watch('deckSize', function() {
				scope.allowAdd = scope.pokemon.count < 4 && scope.deckSize < 60;
				scope.allowSubtract = scope.pokemon.count > 0;
			})
			
			
			scope.addOneCard = function() {
				scope.addYourCard();
				scope.deckSize++;
			};
			
			scope.subtractOneCard = function() {
				scope.subtractYourCard();
				scope.deckSize--;
			};
		},
		templateUrl: "angularjs/print-your-card.html"
	}
}])

