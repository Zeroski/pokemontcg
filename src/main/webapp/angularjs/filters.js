app.filter('stageFilter', function() {
	return function(pokemons, stage) {
		var filtered = [];
		angular.forEach(pokemons, function(pokemon) {
			if (stage == pokemon.stage || stage == null) {
				filtered.push(pokemon);
			}
		});
		return filtered;
	}
})
//.filter('ignoreZeroCounts', function() {
//	return function(pokemons) {
//		var filtered = [];
//		angular.forEach(pokemons, function(pokemon) {
//			if (pokemon) {
//				filtered.push(pokemon);
//			}
//		});
//	}
//})