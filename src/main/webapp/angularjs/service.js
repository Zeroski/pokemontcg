app
.service('cardsService', function(cardsDB) {
	this.getAllCards = function(idArray) {
		return cardsDB.getAllCards(idArray);
	}
})