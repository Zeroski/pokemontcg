
app
.run(['$rootScope', function($rootScope) {
	$rootScope.cache = {
			cards: [],
			yourIndexes: []
	}
	$rootScope.cache.yourIndexes[4] = 0;
	$rootScope.cache.yourIndexes[23] = 1;
}]) 
.controller('myCtrl', function($rootScope, $scope, cardsService) {
	
	var elementMap = {
			fire: [4, 12, 23, 24, 28],
			water: [2, 6, 13, 25],
			grass: [11, 15, 17, 30],
			electric: [9, 14, 16, 20, 21],
			psychic: [1, 10, 29],
			fighting: [7, 8, 19],
			normal: [3, 5, 18, 22, 26, 27],
			energy: [],
			trainer: []
			
	}
	$scope.model = {
			yourDeck: {
				modified: false,
				deckSize: 0,
				inDeck: 0,
				cards: [
				        {
				        	id: 4,
				        	count: 3
				        },
				        {
				        	id: 23,
				        	count: 1
				        }
				        
				        ],
				tempCards: []
			},
			selected: {
				symbol: null,
				cards: [],
				filteredCards: null,
				actualCard: "0",
				filter: null,
				level: null,
			}
	};

    
    $scope.view = {};
    $scope.view.yourCards = false;
    $scope.view.showSavePanel = false;
    
    $scope.$watch('model.yourDeck.cards', function (newVal, oldVal) {
    	countDeckSize();
    }, true);
    
    function countDeckSize() {
    	var count = 0;
    	for(var i = 0; i < $scope.model.yourDeck.cards.length; i++) {
    		count += $scope.model.yourDeck.cards[i].count;
    	}
    	$scope.model.yourDeck.deckSize = count;
    }


    $scope.setLevel = function(level) {
    	$scope.model.selected.level = level;
    }
    
    $scope.selectActualCard = function(cardId) {
    	$scope.model.selected.actualCard = cardId;
    }
    
    $scope.addCard = function(id) {
    	var idx;
    	if (!(angular.isUndefined($rootScope.cache.yourIndexes[id]) || $rootScope.cache.yourIndexes[id] === null)) {
    		var idx = $rootScope.cache.yourIndexes[id];
    		$scope.model.yourDeck.cards[idx].count++;
    	}
    	else {
    		$scope.model.yourDeck.cards.push({id: id, count: 1});
    		$rootScope.cache.yourIndexes[id] = $scope.model.yourDeck.cards.length - 1;
    	}
    	this.card.count++;
    }
    
    $scope.subtractCard = function(id) {
    	var idx = $rootScope.cache.yourIndexes[id];
    	$scope.model.yourDeck.cards[idx].count--;
    	this.card.count--;
    }
    
    $scope.saveChanges = function() {
    	$scope.model.yourDeck.modified = false;
    	$scope.view.showSavePanel = false;
    	$scope.view.yourCards = false;
    	    	
    	$scope.model.yourDeck.tempCards = [];
    	removeZeroCountCards();
    	
    	$scope.setSymbol(null);
    }
    
    $scope.dontSaveChanges = function() {
    	$scope.model.yourDeck.modified = false;
    	$scope.view.showSavePanel = false;
    	$scope.view.yourCards = false;
    	
    	revertChangesInYourCards();
    }
    
    function revertChangesInYourCards() {
    	
    	for (var i = 0; i < $scope.model.yourDeck.tempCards.length; i++) {
    		$scope.model.yourDeck.cards[i].count = $scope.model.yourDeck.tempCards[i].count;
    	}
    	$scope.model.yourDeck.tempCards = [];
    }
    
    $scope.addYourCard = function(idx) {
    	if(!$scope.model.yourDeck.modified) 
    		$scope.model.yourDeck.modified = true;
   
    	$scope.model.yourDeck.cards[idx].count++;
    }
    
    $scope.subtractYourCard = function(idx) {
    	if(!$scope.model.yourDeck.modified) 
    		$scope.model.yourDeck.modified = true;
   
    	$scope.model.yourDeck.cards[idx].count--;
    }
    
    
    $scope.closeYourCards = function() {
    	if($scope.model.yourDeck.modified) {
    		$scope.view.showSavePanel = true;
    	}
    	else {
    		$scope.view.yourCards = false;
    	}
    }
    

    $scope.returnToYourCards = function() {
    	$scope.view.showSavePanel = false;
    }

    $scope.openYourCards = function() {
    	$scope.view.yourCards = true;    	
    	fillTempCards();
    	removeZeroCountCards();
    	
    }
    
    function removeZeroCountCards() {
    	for (var i = $scope.model.yourDeck.cards.length - 1; i >= 0; i--) {
    		if ($scope.model.yourDeck.cards[i].count <= 0) {
    			$scope.model.yourDeck.cards.splice(i, 1);
    		}
    	}
    	updateIndexes();
    }
    
    function updateIndexes() {
    	$rootScope.cache.yourIndexes = [];
    	for (var i = 0; i < $scope.model.yourDeck.cards.length; i++) {
    		var id = $scope.model.yourDeck.cards[i].id;
    		$rootScope.cache.yourIndexes[id] = i;
    	}
    }
    
    
    function fillTempCards() {
    	
    	angular.forEach($scope.model.yourDeck.cards, function(value, key) {
    		$scope.model.yourDeck.tempCards.push({
    			id: value.id,
    			count: value.count
    		});
    	})
    }
    
    $scope.getSelectedCardCount = function() {
    	if ((angular.isUndefined($rootScope.cache.yourIndexes[$scope.model.selected.actualCard]) || $rootScope.cache.yourIndexes[$scope.model.selected.actualCard] === null)) {
    		return "0";
    	}
    	if ($scope.model.selected.actualCard != "0") {
    		var idx = $rootScope.cache.yourIndexes[$scope.model.selected.actualCard];
    		return $scope.model.yourDeck.cards[idx].count;
    	}
    	return "";
    }

    
    $scope.setSymbol = function(obj) {
    	$scope.model.selected.level = null;
    	if (obj)
    		var symbol = obj.currentTarget.attributes.id.value;
    	else {
    		var symbol = $scope.model.selected.symbol;
    	}
    	$scope.model.selected.symbol = symbol;
    	$scope.model.selected.cards = [];
    	checkFilter(); //czy pokazywać filtr w panelu głównym
    	
    	
    	var arrayIds = [];
    	var mapIds = elementMap[symbol]
    	//jeśli karta nie jest w cache to wrzucamy do tablicy
    	for (var i = 0; i < mapIds.length; i++) {
    		if (!$rootScope.cache.cards[mapIds[i]]) {
    			arrayIds.push(mapIds[i]);
    		}
    	}
    	
    	//jeśli coś zebrałem to wołam serwis, żeby pobrał karty i wrzucił do cache
    	if (arrayIds.length > 0) 	{
    		addCardsToCache(arrayIds);
    	}
    	
    	for (var i = 0; i < mapIds.length; i++) {
    		var id = mapIds[i];
    		var idx = $rootScope.cache.yourIndexes[id];
    		
    		if ((idx != null || idx != undefined) && $scope.model.yourDeck.cards.length != 0) {
    			$scope.model.selected.cards.push(
        				{id: $rootScope.cache.cards[id].id, stage: $rootScope.cache.cards[id].stage,
        					count: $scope.model.yourDeck.cards[idx].count} //dla danego id w cache mam index w moich kartach
        			)
    		}
    		else {
    			$scope.model.selected.cards.push(
        				{id: $rootScope.cache.cards[id].id, stage: $rootScope.cache.cards[id].stage,
        					count: 0} //dla danego id w cache mam index w moich kartach
        			)
    		}
    	}
    };
    
    checkFilter = function() {
    	if ($scope.model.selected.symbol == "energy" || $scope.model.selected.symbol == "trainer") {
    		$scope.view.showFilter = false;
    	}  
    	else {
    		$scope.view.showFilter = true;
    	}
    }
    
    function addCardsToCache(arrayIds) {
    	var loadedCards = cardsService.getAllCards(arrayIds);
    	
    	for (var i = 0; i < loadedCards.length; i++) {
    		$rootScope.cache.cards[loadedCards[i].id] = loadedCards[i];
    	}
    }
});