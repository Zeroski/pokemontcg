app
.service('cardsDB', function() {
	var cards = [
			    	{
			    		"id": 1,
			    		"name": "Alakazam",
			    		"symbol": "psychic",
			    		"stage": "2",
			    		"imgSrc": "1.png"
			    	},
			    	{
			    		"id": 2,
			    		"name": "Blastoise",
			    		"symbol": "water",
			    		"stage": "2",
			    		"imgSrc": "2.png"
			    	},
			    	{
			    		"id": 3,
			    		"name": "Chansey",
			    		"symbol": "normal",
			    		"stage": "0",
			    		"imgSrc": "3.png"
			    	},
			    	{
			    		"id": 4,
			    		"name": "Charizard",
			    		"symbol": "fire",
			    		"stage": "2",
			    		"imgSrc": "4.png"
			    	},
			    	{
			    		"id": 5,
			    		"name": "Clefairy",
			    		"symbol": "normal",
			    		"stage": "0",
			    		"imgSrc": "5.png"
			    	},
			    	{
			    		"id": 6,
			    		"name": "Gyarados",
			    		"symbol": "water",
			    		"stage": "1",
			    		"imgSrc": "6.png"
			    	},
			    	{
			    		"id": 7,
			    		"name": "Hitmonchan",
			    		"symbol": "fighting",
			    		"stage": "0",
			    		"imgSrc": "7.png"
			    	},
			    	{
			    		"id": 8,
			    		"name": "Machamp",
			    		"symbol": "fighting",
			    		"stage": "2",
			    		"imgSrc": "8.png"
			    	},
			    	{
			    		"id": 9,
			    		"name": "Magneton",
			    		"symbol": "electric",
			    		"stage": "1",
			    		"imgSrc": "9.png"
			    	},
			    	{
			    		"id": 10,
			    		"name": "Mewtwo",
			    		"symbol": "psychic",
			    		"stage": "0",
			    		"imgSrc": "10.png"
			    	},
			    	{
			    		"id": 11,
			    		"name": "Nidoking",
			    		"symbol": "grass",
			    		"stage": "2",
			    		"imgSrc": "11.png"
			    	},
			    	{
			    		"id": 12,
			    		"name": "Ninetales",
			    		"symbol": "fire",
			    		"stage": "1",
			    		"imgSrc": "12.png"
			    	},
			    	{
			    		"id": 13,
			    		"name": "Poliwrath",
			    		"symbol": "water",
			    		"stage": "2",
			    		"imgSrc": "13.png"
			    	},
			    	{
			    		"id": 14,
			    		"name": "Raichu",
			    		"symbol": "electric",
			    		"stage": "1",
			    		"imgSrc": "14.png"
			    	},
			    	{
			    		"id": 15,
			    		"name": "Venusaur",
			    		"symbol": "grass",
			    		"stage": "2",
			    		"imgSrc": "15.png"
			    	},
			    	{
			    		"id": 16,
			    		"name": "Zapdos",
			    		"symbol": "electric",
			    		"stage": "0",
			    		"imgSrc": "16.png"
			    	},
			    	{
			    		"id": 17,
			    		"name": "Beedrill",
			    		"symbol": "grass",
			    		"stage": "2",
			    		"imgSrc": "17.png"
			    	},
			    	{
			    		"id": 18,
			    		"name": "Dragonair",
			    		"symbol": "normal",
			    		"stage": "1",
			    		"imgSrc": "18.png"
			    	},
			    	{
			    		"id": 19,
			    		"name": "Dugtrio",
			    		"symbol": "fighting",
			    		"stage": "1",
			    		"imgSrc": "19.png"
			    	},
			    	{
			    		"id": 20,
			    		"name": "Electabuzz",
			    		"symbol": "electric",
			    		"stage": "0",
			    		"imgSrc": "20.png"
			    	},
			    	{
			    		"id": 21,
			    		"name": "Electrode",
			    		"symbol": "electric",
			    		"stage": "1",
			    		"imgSrc": "21.png"
			    	},
			    	{
			    		"id": 22,
			    		"name": "Pidgeotto",
			    		"symbol": "normal",
			    		"stage": "1",
			    		"imgSrc": "22.png"
			    	},
			    	{
			    		"id": 23,
			    		"name": "Arcanaine",
			    		"symbol": "fire",
			    		"stage": "1",
			    		"imgSrc": "23.png"
			    	},
			    	{
			    		"id": 24,
			    		"name": "Charmeleon",
			    		"symbol": "fire",
			    		"stage": "1",
			    		"imgSrc": "24.png"
			    	},
			    	{
			    		"id": 25,
			    		"name": "Dewgong",
			    		"symbol": "water",
			    		"stage": "1",
			    		"imgSrc": "25.png"
			    	},
			    	{
			    		"id": 26,
			    		"name": "Dratini",
			    		"symbol": "normal",
			    		"stage": "0",
			    		"imgSrc": "26.png"
			    	},
			    	{
			    		"id": 27,
			    		"name": "Farfetch'd",
			    		"symbol": "normal",
			    		"stage": "0",
			    		"imgSrc": "27.png"
			    	},
			    	{
			    		"id": 28,
			    		"name": "Growlithe",
			    		"symbol": "fire",
			    		"stage": "0",
			    		"imgSrc": "28.png"
			    	},
			    	{
			    		"id": 29,
			    		"name": "Haunter",
			    		"symbol": "psychic",
			    		"stage": "1",
			    		"imgSrc": "29.png"
			    	},
			    	{
			    		"id": 30,
			    		"name": "Ivysaur",
			    		"symbol": "grass",
			    		"stage": "1",
			    		"imgSrc": "30.png"
			    	},
			    ];
	this.getAllCards = function(idArray) {
		
		var selectedCards = [];
		
		idArray.forEach(function(id) {
			selectedCards.push(cards[id - 1]);
		})
		
		
		return selectedCards;
	};
});

